{{
  const _DEFAULT_OPTIONS = {
    PHONE: { DEFAULT_COUNTRY_CODE: 1 },
    FIRST_DAY_OF_WEEK: 0 // moment.localeData()._week.dow
  }
  // ISO days of the week
  const _DAYS_OF_THE_WEEK = [ "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" ]

  // momentjs to osm mapping3
  function osmDay(n) {
    return(  [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ][n%7] );
  }
}}

{
  // Override dedault options
  options = Object.assign( {}, _DEFAULT_OPTIONS, options )
  
  // Assign locale days of the week
  const _DAYS_OF_THE_WEEK_LOCALE = _DAYS_OF_THE_WEEK.map( (cur,i,arr) => arr[ (i+options.FIRST_DAY_OF_WEEK+6) % 7 ] )
  
  function warning(message, location=undefined, notes=undefined) {
    console.warn(message);
  }
}

start = _* @(phone / opening_hours) _*

// Accept, and format phone numbers
// Return in osm format : [+]\d-\d\d\d-\d\d\d-\d\d\d\d
// https://wiki.openstreetmap.org/wiki/Key:phone

phone = 
  digits: ( (  &DIGIT|11| @DIGIT @DIGIT|3| @DIGIT|3| @DIGIT|4| ) /
	    ( @&DIGIT|10|        @DIGIT|3| @DIGIT|3| @DIGIT|4| ) /
	    // Dash delimited, optional country code
	    ( @( "+"? @DIGIT DASH)? @DIGIT|3| DASH @DIGIT|3| DASH @DIGIT|4|) /
	    // Whitespace delimited, optional country code
	    ( @( "+"? @DIGIT  _+)? @DIGIT|3| _+ @DIGIT|3| _+ @DIGIT|4|) / 
	    // General any delimiter
	    ( @( @DIGIT)? (!DIGIT .)* @DIGIT|3| (!DIGIT .)* @DIGIT|3| (!DIGIT .)* @DIGIT|4|)
	  )
{
  //return digits;
  
  // Expects [ COUNTRY_CODE_OR_NULL, [3_DIGITS], [3_DIGITS], [4_DIGITS] ]
  // Format and return
  return ( '+' +
	   [ digits[0] ?? options.PHONE.DEFAULT_COUNTRY_CODE,
	     ...digits.slice(1).map(d => d.join(""))
	   ].join('-')
	 )
}

// Parse and format opening_hours
opening_hours "opening_hours" 
  = opening_hours:(
    // Accept list of days, or day range, a colon, then time interval
    @DAYS_OF_WEEK _* [:]? _*  @(TIME_OF_DAY_INTERVAL / opening_hours_OFF)
  )|.., _* [ ,;]? _* |
  {
    // Expect array like [ [days, times], ... ]
    var result = Array(7).fill(null)
    
    return opening_hours.map( ([days, times]) => 
      Array.isArray(days) ? days.join(',') + " " + times
	: `${days.start}-${days.end}` + " " + times
    ).join('; ')
  }

// keywerds for when a buisness is closed
opening_hours_OFF
  = (
    "CLOSED"i
  )
{
  return "off"
}


// Match a singleton, list, or range of days of the week
// Return a [ day ] for singleton
// Return a [ day1, day2, ... ] for list
// Return a { start:s, end:e } for a range
DAYS_OF_WEEK  "DAYS_OF_WEEK"
  = (
    s:DAY_OF_WEEK interval_delimeter e:DAY_OF_WEEK { return ( { start:s, end:e } ) } /
      DAY_OF_WEEK|1..,_* "," _*|
  )

// Parse day of week
// Format and return osm format Mo, Tu, We, Th, Fr, Sa, Su
DAY_OF_WEEK "DAY_OF_WEEK" = a: (
  // Dumb test with startsWith, Require at least two letters.  Ignore misspellings
  a: $LETTER|2..| { return _DAYS_OF_THE_WEEK.find( e => a.toUpperCase().startsWith(e.toUpperCase()) ) } /
    // One Letter
  ( "M"i { return "Mo" } /
    "T"i { return "Tu" } /
    "W"i { return "We" } /
    "R"i { return "Th" } /
    "F"i { return "Fr" } /
    "S"i { return "Sa" } /
    "U"i { return "Su" }
  )
)
{
  if (!a) error("Invalid DAY_OF_WEEK");
  return a
}

// Match a single interval of time
// Expects [ [ h, m, AM/PM ], [ h, m, AM/PM ] ]
// If AM/PM is unspecified, make a good guess and warn
// Return in osm format hh:mm-hh:mm
TIME_OF_DAY_INTERVAL = start:TIME_OF_DAY interval_delimeter end:TIME_OF_DAY
{
  return(( [sth, stm, sta], [edh, edm, eda] ) => {
    if(sta == undefined || eda == undefined)
      warning("AM/PM was not specified");
    
    return(
      [ (sta == "PM" && sth < 12 ? sth + 12 : sth).toString().padStart(2,'0'),
	':',
	stm.toString().padStart(2,'0'),
	'-',
	(eda == "PM" && edh < 12 ? edh + 12 : edh).toString().padStart(2,'0'),
	':',
	edm.toString().padStart(2,'0')
      ].join("")
    )
  })(start, end);
}

// Acceprable delimeters for ranges
interval_delimeter = _* DASH _* /
  _+ ( "TO"i / "THRU" ) _+        

// Parse a time of day
// Return as [ h, m,  AM/PM/undefined ]
// No assumptions are made about AM/PM. Leave it up to the formatter
TIME_OF_DAY "TIME_OF_DAY"
  = h:$DIGIT|1..2| m:(":" @$DIGIT|2| / @"")? _* a:(AM/PM)?
  {
    var result = [ parseInt(h), m ? parseInt(m) : 0, a ? a.toUpperCase() : undefined ];
    return result;
  }

AM = "AM"i / "A.M."i
PM = "PM"i / "P.M."i

DIGITS = DIGIT+

DIGIT "DIGIT" = [0-9]

LETTERS = LETTER+
  
LETTER "LETTER" = [A-Z]i

// Match any dash
DASH = MINUS / HYPHEN / EN_DASH / EM_DASH / HORIZONTAL_BAR / FIGURE_DASH

MINUS = '-'
HYPHEN = "‐"
EN_DASH = "–"
EM_DASH = "—"
HORIZONTAL_BAR = "―"
FIGURE_DASH = "‒"

_ "WHITESPACE"
  = [ \t\n\r]
