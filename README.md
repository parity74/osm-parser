# osm-parser

parser & formatter for osm tags:
- [phone](https://wiki.openstreetmap.org/wiki/Key:phone)
- [opening_hours](https://wiki.openstreetmap.org/wiki/Key:opening_hours)

## Install node & dependencies

1. Install [node.js](https://nodejs.org/)
2. Install [peggyjs](https://peggyjs.org/) `npm install peggy`

## osm-parser.sh
Parse and format clipboard

Linux: Requires xclip `sudo apt-get install xclip`



Mac (TO-DO)

Probably needs `pbcopy` and `pbpaste`



Windows: `¯\_(ツ)_/¯`
