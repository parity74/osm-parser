{{
  const DAYS_OF_THE_WEEK = [ "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" ]

  const DEFAULT_OPTIONS = {
    PHONE: { DEFAULT_COUNTRY_CODE: 1 }
  }
}}

{
  options = Object.assign( {}, DEFAULT_OPTIONS, options )
  function warning(message, location=undefined, notes=undefined) {
    console.warn(message);
  }
}

start = _* @(phone) _*

// Accept, and format phone numbers
// Return in osm format : [+]\d-\d\d\d-\d\d\d-\d\d\d\d
// https://wiki.openstreetmap.org/wiki/Key:phone

phone = 
  digits: ( (  &DIGIT|11| @DIGIT @DIGIT|3| @DIGIT|3| @DIGIT|4| ) /
	    ( @&DIGIT|10|        @DIGIT|3| @DIGIT|3| @DIGIT|4| ) /
	    // Dash delimited, optional country code
	    ( @( "+"? @DIGIT DASH)? @DIGIT|3| DASH @DIGIT|3| DASH @DIGIT|4|) /
	    // Whitespace delimited, optional country code
	    ( @( "+"? @DIGIT  _+)? @DIGIT|3| _+ @DIGIT|3| _+ @DIGIT|4|) / 
	    // General any delimiter
	    ( @( @DIGIT)? (!DIGIT .)* @DIGIT|3| (!DIGIT .)* @DIGIT|3| (!DIGIT .)* @DIGIT|4|)
	  )
{
  //return digits;
  
  // Expects [ COUNTRY_CODE_OR_NULL, [3_DIGITS], [3_DIGITS], [4_DIGITS] ]
  // Format and return
  return ( '+' +
	   [ digits[0] ?? options.PHONE.DEFAULT_COUNTRY_CODE,
	     ...digits.slice(1).map(d => d.join(""))
	   ].join('-')
	 )
}

DIGITS = DIGIT+

DIGIT "DIGIT" = [0-9]

// Match any dash
DASH = MINUS / HYPHEN / EN_DASH / EM_DASH / HORIZONTAL_BAR / FIGURE_DASH
  
MINUS = '-'
HYPHEN = "‐"
EN_DASH = "–"
EM_DASH = "—"
HORIZONTAL_BAR = "―"
FIGURE_DASH = "‒"

_ "WHITESPACE"
  = [ \t\n\r]
